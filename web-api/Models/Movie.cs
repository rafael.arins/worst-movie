using System;
using System.Collections.Generic;

namespace web_api.Models
{
    public class Movie
    {
        public Movie()
        {
            this.Id = new Guid();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public ICollection<MovieStudio> MovieStudios { get; set; }
        public ICollection<MovieProducer> MovieProducers { get; set; }
        public bool isWinner { get; set; }
    }
}