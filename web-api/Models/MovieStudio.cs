using System;

namespace web_api.Models
{
    public class MovieStudio
    {
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
        public Guid StudioId { get; set; }
        public Studio Studio { get; set; }
    }
}