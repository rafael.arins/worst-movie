using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace web_api.db.InMemory
{
    public class InMemoryContext : DbContext
    {
        public InMemoryContext(DbContextOptions<InMemoryContext> options)
            : base(options) { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Studio> Studios { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<MovieStudio> MovieStudios { get; set; }
        public DbSet<MovieProducer> MovieProducer { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieStudio>()
                .HasKey(ms => new { ms.MovieId, ms.StudioId });
            modelBuilder.Entity<MovieStudio>()
                .HasOne(ms => ms.Movie)
                .WithMany(m => m.MovieStudios)
                .HasForeignKey(ms => ms.MovieId);
            modelBuilder.Entity<MovieStudio>()
                .HasOne(ms => ms.Studio)
                .WithMany(s => s.MovieStudios)
                .HasForeignKey(ms => ms.StudioId);

            modelBuilder.Entity<MovieProducer>()
                .HasKey(mp => new { mp.MovieId, mp.ProducerId });
            modelBuilder.Entity<MovieProducer>()
                .HasOne(mp => mp.Movie)
                .WithMany(m => m.MovieProducers)
                .HasForeignKey(mp => mp.MovieId);
            modelBuilder.Entity<MovieProducer>()
                .HasOne(mp => mp.Producer)
                .WithMany(p => p.MovieProducers)
                .HasForeignKey(mp => mp.ProducerId);
        }
    }
}