using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using web_api.db.InMemory;

namespace web_api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudioController : ControllerBase
    {
        private readonly InMemoryContext _context;

        public StudioController(InMemoryContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get All Studios order by the number of wins
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/studio
        ///
        /// </remarks>
        /// <returns>List of studio's name and winnig count</returns>
        /// <response code="200">Returns the list or empty</response>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get()
        {
            var studios = await _context.Studios
                .Include(s => s.MovieStudios)
                .GroupBy(s => s.Name, (key, group) => new
                {
                    name = key,
                    winnerCount = group.Count()
                })
                .OrderByDescending(s => s.winnerCount)
                .ToArrayAsync();

            return Ok(new { studios = studios });
        }

    }
}