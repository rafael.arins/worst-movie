using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using web_api.db.InMemory;

namespace web_api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProducerController : ControllerBase
    {

        private readonly InMemoryContext _context;

        public ProducerController(InMemoryContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get the largest and shortest interval between producer victories.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/producer/ProducerMaxMinInterval
        ///
        /// </remarks>
        /// <returns>A object with de max e min interval.</returns>
        /// <response code="200">Returns the object or empty</response>
        [HttpGet("[action]")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> ProducerMaxMinInterval()
        {

            var producerOneMoreWin = await _context.Producers
                .Select(p => new
                {
                    p.Name,
                    movie = p.MovieProducers.Where(mp => mp.Movie.isWinner)
                        .Select(mp => mp.Movie)
                        .FirstOrDefault()
                }).GroupBy(t => t.Name, (key, gruop) => new
                {
                    name = key,
                    movies = gruop.Where(tt => tt.movie != null)
                        .Select(tt => tt.movie)
                        .OrderBy(tt => tt.ReleaseDate)
                        .ToArray()
                }).Where(t => t.movies.Count() > 1).ToArrayAsync();


            var maxInterval = new
            {
                producer = "",
                interval = -1,
                previousWin = -1,
                followingWin = -1
            };

            var minInterval = maxInterval;

            foreach (var producer in producerOneMoreWin)
            {
                int currentMovie = 0;
                int nextMovie = 0;

                for (nextMovie = currentMovie + 1; nextMovie < producer.movies.Count(); nextMovie++)
                {
                    int interval = producer.movies[nextMovie].ReleaseDate.Year -
                        producer.movies[currentMovie].ReleaseDate.Year;

                    var currentProducer = new
                    {
                        producer = producer.name,
                        interval = interval,
                        previousWin = producer.movies[currentMovie].ReleaseDate.Year,
                        followingWin = producer.movies[nextMovie].ReleaseDate.Year
                    };

                    if (maxInterval.interval == -1 || maxInterval.interval < currentProducer.interval)
                        maxInterval = currentProducer;

                    if (minInterval.interval == -1 || minInterval.interval > currentProducer.interval)
                        minInterval = currentProducer;

                    currentMovie++;
                }
            }


            return Ok(new { max = maxInterval, min = minInterval });
        }

    }
}