# Worst Movie API

API RESTful para possibilitar a leitura e manutenção da lista de indicados e vencedores da categoria Pior Filme do Golden Raspberry Awards.

Desenvolvido com .Net Core 2 no Visual Code.

## Estrutura do projeto
<pre>
/worst-web-api\
    /.vscode            -- contem os arquivos de configurações para o porjeto poder ser depurado pelo Visal Code.
    /web-api            -- O projeto da API.
    /web-api.test       -- Os teste de integração da API.
    worst-movie-api.sln -- Arquivo de solução do projeto.
</pre>

## Uso

### .NET CLI

Navegue até a pasta raiz do projeto 'worst-movie-api' e use o commando abaixo no promt de comando para instalar o pacotes necessarios.

```bash
dotnet restore
```

Para executar a API vá para a pasta 'worst-movie-api\web-api' e use o comando : 

```bash
dotnet run
```

Para executar os teste de integração acesse a pasta 'worst-movie-api/web-api.test' e use o comando:

```bash
dotnet test
```